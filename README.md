Hello!

This is SketchDeck's official bootstrap theme. Use this in all our websites!

This theme is a bit out of date and needs some love. That will happen. USE IT ANYWAY.

By using this template pretty much all standard HTML and bootstrap elements (p, h*, .btn) should look good. 

Try to write AS LITTLE CUSTOM CSS as possible, use existing stuff here when you can.